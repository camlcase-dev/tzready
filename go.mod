module gitlab.com/camlcase-dev/tzready

go 1.14

require (
	github.com/caarlos0/env/v6 v6.5.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-resty/resty/v2 v2.5.0
	github.com/goat-systems/go-tezos/v4 v4.0.4
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
)
