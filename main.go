package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/tzready/internal/config"
	"gitlab.com/camlcase-dev/tzready/internal/readiness"
	"gitlab.com/camlcase-dev/tzready/internal/server"
	"gitlab.com/camlcase-dev/tzready/internal/slack"
)

func main() {
	logger := logrus.New()
	cfg, err := config.New()
	if err != nil {
		logger.WithField("error", err.Error()).Fatal("Failed to load configuration.")
	}

	slackClient := slack.New(cfg.SlackWebhook)

	// Loop until a successful connection with reference nodes.
	started := false
	var rp *readiness.Probe
	for !started {
		rp, err = readiness.Start(cfg, slackClient, logger)
		if err != nil {
			logger.WithField("error", err.Error()).Error("Failed to start readiness probe.")
		} else {
			started = true
		}
	}

	err = server.New(cfg.Port, rp, logger).Start()
	if err != nil {
		logger.WithField("error", err.Error()).Fatal("Failed to start server.")
	}
}
