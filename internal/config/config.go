package config

import (
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// Config is tzbalance's configuration
type Config struct {
	Port             int           `env:"TZREADY_PORT" envDefault:"8080"`
	ReferenceNodes   []string      `env:"TZREADY_REFERENCE_NODES" envSeparator:"," validate:"required"`
	HostNode         string        `env:"TZREADY_HOST_NODE"  envDefault:"http://localhost:8732"`
	GroupName        string        `env:"TZREADY_GROUP_NAME" validate:"required"`
	SyncInterval     time.Duration `env:"TZREADY_SYNC_INTERVAL" envDefault:"1m"`
	SlackWebhook     string        `env:"TZREADY_SLACK_WEBHOOK" validate:"required"`
	ReferenceTrigger int           `env:"TZREADY_REFERENCE_TRIGGER" envDefault:"30" validate:"required"`
	BackendTrigger   int           `env:"TZREADY_BACKEND_TRIGGER" envDefault:"30" validate:"required"`
	StateTrigger     int           `env:"TZREADY_STATE_TRIGGER" envDefault:"30" validate:"required"`
}

// New loads enviroment variables into a Config struct
func New() (Config, error) {
	config := Config{}
	if err := env.Parse(&config); err != nil {
		return config, errors.Wrap(err, "failed to load enviroment variables")
	}

	err := validator.New().Struct(&config)
	if err != nil {
		return config, errors.Wrap(err, "configuration validation failed")
	}

	return config, nil
}
