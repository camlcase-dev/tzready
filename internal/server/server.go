package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/tzready/internal/readiness"
)

// Server is the http proxy to our node
type Server struct {
	port   int
	logger *logrus.Logger
	rp     *readiness.Probe
}

// New returns a new server
func New(port int, rp *readiness.Probe, logger *logrus.Logger) *Server {
	return &Server{
		port:   port,
		logger: logger,
		rp:     rp,
	}
}

// Start starts an http server that forwards requests to a node.
func (s *Server) Start() error {

	http.HandleFunc("/healthz", s.handler)
	s.logger.Infof("Listening on port (%d)", s.port)
	return http.ListenAndServe(fmt.Sprintf(":%d", s.port), nil)
}

func (s *Server) handler(w http.ResponseWriter, r *http.Request) {
	ready := s.rp.GetReadiness()
	if ready {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusServiceUnavailable)
	}
	json.NewEncoder(w).Encode(ready)
}
