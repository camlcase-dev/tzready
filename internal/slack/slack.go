package slack

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// Slack API with webhook
type Slack struct {
	webhook string
}

// New returns a new slack
func New(webhook string) *Slack {
	return &Slack{
		webhook: webhook,
	}
}

// requestBody is the body to send to slack
type requestBody struct {
	Text string `json:"text"`
}

// Send sends a message to slack
func (s *Slack) Send(msg string) error {
	slackBody, _ := json.Marshal(requestBody{Text: msg})
	req, err := http.NewRequest(http.MethodPost, s.webhook, bytes.NewBuffer(slackBody))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to send to slack")
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	if buf.String() != "ok" {
		return errors.New("failed to send to slack")
	}
	return nil
}
