package readiness

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/goat-systems/go-tezos/v4/rpc"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/camlcase-dev/tzready/internal/config"
	"gitlab.com/camlcase-dev/tzready/internal/slack"
)

// Probe -
type Probe struct {
	logger            *logrus.Logger
	slackClient       *slack.Slack
	cfg               config.Config
	tzclient          rpc.IFace
	rclients          []rpc.IFace
	referenceFailures int
	backendFailures   int
	stateFailures     int
	lastHeight        int
	ready             bool
	mu                *sync.Mutex
}

// Start starts the sync process of heights between nodes
func Start(cfg config.Config, slackClient *slack.Slack, logger *logrus.Logger) (*Probe, error) {
	tzclient, err := rpc.New(cfg.HostNode)
	if err != nil {
		return nil, errors.Wrap(err, "failed to start readiness probe")
	}

	var rclients []rpc.IFace
	for _, reference := range cfg.ReferenceNodes {
		r, err := rpc.New(reference)
		if err != nil {
			return nil, errors.Wrap(err, "failed to start readiness probe")
		}
		rclients = append(rclients, r)
	}

	rp := &Probe{
		logger:      logger,
		cfg:         cfg,
		tzclient:    tzclient,
		rclients:    rclients,
		slackClient: slackClient,
		mu:          &sync.Mutex{},
	}

	rp.start()

	return rp, nil
}

func (r *Probe) start() {
	r.logger.Info("Starting readiness probe.")
	go func() {
		r.checkState()

		ticker := time.NewTicker(r.cfg.SyncInterval)
		for range ticker.C {
			r.checkState()
		}
	}()
}

func (r *Probe) checkState() {
	var knownHeights []int
	for _, client := range r.rclients {
		resp, block, err := client.Block(&rpc.BlockIDHead{})
		if err != nil {
			r.handleError(resp, err)
			r.referenceFailures++
			r.maybeReferenceWarn()
			return
		}
		knownHeights = append(knownHeights, block.Header.Level)
	}
	r.referenceFailures = 0

	resp, head, err := r.tzclient.Block(&rpc.BlockIDHead{})
	if err != nil {
		r.handleError(resp, err)
		r.backendFailures++
		r.maybeBackendWarn()
		return
	}
	r.backendFailures = 0

	if len(knownHeights) > 0 {
		sort.Ints(knownHeights)
		height := knownHeights[len(knownHeights)-1]

		r.logger.WithField("level", height).Info("Found current tezos block height.")

		if r.lastHeight == head.Header.Level {
			r.stateFailures++
			r.maybeStateWarn()
		}

		if height == head.Header.Level {
			r.logger.WithField("level", height).Info("Tezos node is in sync.")
			r.mu.Lock()
			r.ready = true
			r.mu.Unlock()
		} else {
			r.logger.WithFields(logrus.Fields{
				"head-level": height,
				"node-level": head.Header.Level,
			}).Warn("Tezos node is not in sync.")
			r.mu.Lock()
			r.ready = false
			r.mu.Unlock()
		}
	}
}

func (r *Probe) maybeReferenceWarn() {
	if r.referenceFailures == r.cfg.ReferenceTrigger {
		r.logger.Error("Unable to reach out to reference nodes.")
		err := r.slackClient.Send(fmt.Sprintf("[%s] Multiple failures to get state against a reference node.", r.cfg.GroupName))
		if err != nil {
			r.logger.WithFields(logrus.Fields{
				"error": err.Error(),
			}).Error("Failed to post slack message.")
		}
		r.referenceFailures = 0
	}
}

func (r *Probe) maybeBackendWarn() {
	if r.backendFailures == r.cfg.BackendTrigger {
		r.logger.Error("Unable to get state from node.")
		err := r.slackClient.Send(fmt.Sprintf("[%s] Multiple failures to get state against a backend node.", r.cfg.GroupName))
		if err != nil {
			r.logger.WithFields(logrus.Fields{
				"error": err.Error(),
			}).Error("Failed to post slack message.")
		}
		r.backendFailures = 0
	}
}

func (r *Probe) maybeStateWarn() {
	if r.stateFailures == r.cfg.StateTrigger {
		r.logger.Error("Node is not syncing.")
		err := r.slackClient.Send(fmt.Sprintf("[%s] A backend node is not syncing correctly.", r.cfg.GroupName))
		if err != nil {
			r.logger.WithFields(logrus.Fields{
				"error": err.Error(),
			}).Error("Failed to post slack message.")
		}
		r.stateFailures = 0
	}
}

func (r *Probe) handleError(resp *resty.Response, err error) {
	if resp != nil {
		r.logger.WithFields(logrus.Fields{
			"error":       err.Error(),
			"resp-status": resp.Status(),
			"resp-body":   string(resp.Body()),
			"resp-url":    resp.Request.URL,
		}).Error("Failed to get head block.")
	} else {
		r.logger.WithFields(logrus.Fields{
			"error": err.Error(),
		}).Error("Failed to get head block.")
	}
}

// GetReadiness returns whether a node is ready or not
func (r *Probe) GetReadiness() bool {
	r.mu.Lock()
	defer r.mu.Unlock()
	return r.ready
}
