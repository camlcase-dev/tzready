# Tzready

Tzready is a readiness probe for tezos-nodes behind a kubernetes loadbalancer. It will check the state of a group of reference nodes, get the maximum reported height and then compare the height with the backend node to decide readiness. 

## Configuration
| Env Var                   | Required/Default           | Description                                                                                                |
|---------------------------|----------------------------|------------------------------------------------------------------------------------------------------------|
| TZREADY_PORT              | False/8080                 | The port you want to run the readiness probe on.                                                           |
| TZREADY_REFERENCE_NODES   | True/NA                    | The reference nodes (,) separated.                                                                         |
| TZREADY_HOST_NODE         | True/http://localhost:8732 | The node you want to check against the reference nodes.                                                    |
| TZREADY_GROUP_NAME        | True/NA                    | A unique name for the host node for slack notification purposes.                                           |
| TZREADY_SYNC_INTERVAL     | False/1m                   | The interval at which the state of the host node is checked against the state of the reference nodes.      |
| TZREADY_SLACK_WEBHOOK     | True/NA                    | The slack webhook to send notifications too.                                                               |
| TZREADY_REFERENCE_TRIGGER | False/30                   | If we are unable to get the state from a reference node in 30 tries in a row send a notification to slack. |
| TZREADY_BACKEND_TRIGGER   | False/30                   | If we are unable to get the block from a backend node 30 tries in a row send a notification to slack.      |
| TZREADY_STATE_TRIGGER     | False/30                   | If we are unable to get the state from a backend node 30 tries in a row send a notification to slack.      |


## Example K8s
```
- name: tezos-readiness-probe
  image: camlcasetezos/tzready:v0.1.0
  imagePullPolicy: Always
  command: ["tzready"]
  ports:
  - containerPort: 8080
  env:
    - name: TZREADY_REFERENCE_NODES
      value: "https://mainnet-tezos.giganode.io,https://rpc.tzbeta.net"
    - name: TZREADY_GROUP_NAME
      value: "PUBLIC_FULL_NODES"
    - name: TZREADY_SYNC_INTERVAL
      value: "1s"
    - name: TZREADY_SLACK_WEBHOOK
      value: "https://hooks.slack.com/services/<TODO>/<TODO>/<TODO>"
    - name: TZREADY_REFERENCE_TRIGGER
      value: "10"
    - name: TZREADY_BACKEND_TRIGGER
      value: "10"
    - name: TZREADY_STATE_TRIGGER
      value: "300"
  readinessProbe:
      httpGet:
      path: /healthz
      port: 8080
      initialDelaySeconds: 30
      periodSeconds: 1
```

## Run Locally
```
docker-compose up --build
```

## [LICENSE.md](License.md)